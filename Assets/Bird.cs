﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class Bird : MonoBehaviour
{
	public bool m_isAlive;
	public TextMesh m_textChant;
	GameObject m_objectArrow;
	GameObject m_objectArrow2;
	public GameObject m_rootHeadshot;

	public int hp = 1;
	int currenthp = 0;

	bool ExplodeScore(){
		if (m_objectArrow.GetComponent<Rocket> ()) {
			this.GetComponent<SpriteRenderer> ().color = new Color (0, 0, 0);
			m_objectArrow.SetActive (false);
			GameScene.instance.HitExplode (this.transform.position);
			return true;
		} else {
			
			return false;
		}
	}
	//void OnCollisionEnter2D(Collision2D coll) {
	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Arrow" && m_isAlive) {

			currenthp--;
			if (currenthp <= 0) {
				m_isAlive = false;

				this.GetComponent<Animator> ().Stop ();

				m_textChant.text = "";
				m_objectArrow = coll.gameObject;
				if (Vector3.Distance (m_rootHeadshot.transform.position, m_objectArrow.transform.position) < 0.3f) {
					//Debug.LogError ("Headshot!");
					//if (m_objectArrow.GetComponent<Rocket> ()) {
					//} else {
						GameScene.instance.HitHeadshot (this.transform.position);
					//}

					if (ExplodeScore ()) {
						this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range(-400,400), Random.Range(200,400)));
						this.GetComponent<Rigidbody2D> ().gravityScale = 0.8f;
						this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (750, 900));
					}else if (Random.Range (0, 100) < 70) {
						this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, -500));
						this.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (3, 6);
						this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-900, -1000));
					} else {
						this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (100, 500));
						this.GetComponent<Rigidbody2D> ().gravityScale = 2;
						this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-900, -1000));
					}
					GameScene.instance.Score (2);


				} else {
					ExplodeScore ();
					GameScene.instance.Score (1);

					if (ExplodeScore ()) {
						this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (Random.Range(-400,400), Random.Range(200,400)));
						this.GetComponent<Rigidbody2D> ().gravityScale = 0.8f;
						this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (50, 100));
					}else if (Random.Range (0, 100) < 70) {
						this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, -500));
						this.GetComponent<Rigidbody2D> ().gravityScale = Random.Range (3, 6);
						this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (0, -400));
					} else {
						this.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (100, 500));
						this.GetComponent<Rigidbody2D> ().gravityScale = 2;
						this.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (0, -400));
					}
				}
					
				GameScene.instance.m_listBirds.Remove (this.gameObject);
			} else {
				m_objectArrow2 = coll.gameObject;
				if (Vector3.Distance (m_rootHeadshot.transform.position, m_objectArrow2.transform.position) < 0.4f) {
					Debug.LogError ("Headshot!");
					GameScene.instance.HitHeadshot (this.transform.position);
				}
				this.GetComponent<Rigidbody2D> ().velocity = this.GetComponent<Rigidbody2D> ().velocity + new Vector2 (-1, 0);

			}

			if (m_objectArrow.GetComponent<Rocket> ()) {
			} else {
				LeanTween.delayedCall (0.03f, HitRoutine);
				GameScene.instance.Hit (this.transform.position);
			}


			/*if( coll.transform.position.x > this.transform.position.x - 1 && 
			   coll.transform.position.x < this.transform.position.x - 0.5f )
			{	GameScene.instance.HitHeadshot(this.transform.position);
			}*/
		}
		//Debug.LogError ("Trigger Enter");
	}

	void HitRoutine()
	{
		if (m_objectArrow) {
			m_objectArrow.transform.parent = this.transform;
			m_objectArrow.GetComponent<BoxCollider2D> ().enabled = false;
			m_objectArrow.transform.localEulerAngles += new Vector3 (0, 0, 45);
			m_objectArrow.transform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
			m_objectArrow.GetComponent<Rigidbody2D> ().isKinematic = true;
		}
		if (m_objectArrow2) {
			m_objectArrow2.transform.parent = this.transform;
			m_objectArrow2.GetComponent<BoxCollider2D> ().enabled = false;
			m_objectArrow2.transform.localEulerAngles += new Vector3 (0, 0, 45);
			m_objectArrow2.transform.localScale = new Vector3 (0.8f, 0.8f, 0.8f);
			m_objectArrow2.GetComponent<Rigidbody2D> ().isKinematic = true;
		}

	}

	public void SetChant(int currentbird)
	{
		int chatvalue = Random.Range (0, 5);
		if(currentbird == 0 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "One Big Fight!";
				break;
			case 1 : 
				m_textChant.text = "Fight Blue and White!";
				break;
			case 2 : 
				m_textChant.text = "Go Blue Eagle!";
				break;
			default : 
				m_textChant.text = "One Big Fight!";
				break;
			}
		}
		else if(currentbird == 1 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Fight!";
				break;
			case 1 : 
				m_textChant.text = "Shout and Cheer!";
				break;
			case 2 : 
				m_textChant.text = "Hey hey!";
				break;
			default : 
				m_textChant.text = "Maroons!";
				break;
			}
		}
		else if(currentbird == 2 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Fight!";
				break;
			case 1 : 
				m_textChant.text = "Go Fight!";
				break;
			case 2 : 
				m_textChant.text = "Victory!";
				break;
			default : 
				m_textChant.text = "Charge!";
				break;
			}
		}
		else if(currentbird == 3 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Black gold black white!";
				break;
			case 1 : 
				m_textChant.text = "Animo Cheers!";
				break;
			case 2 : 
				m_textChant.text = "Tamaraw!";
				break;
			default : 
				m_textChant.text = "Power!";
				break;
			}
		}
		else if(currentbird == 4 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Go Fight";
				break;
			case 1 : 
				m_textChant.text = "Warriors!";
				break;
			case 2 : 
				m_textChant.text = "Go Go Go!";
				break;
			default : 
				m_textChant.text = "Go Fight Red and White!";
				break;
			}
		}
		else if(currentbird == 5 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Go Go Go";
				break;
			case 1 : 
				m_textChant.text = "Cheer!";
				break;
			case 2 : 
				m_textChant.text = "Lets Go!";
				break;
			default : 
				m_textChant.text = "Bulldogs!";
				break;
			}
		}
		else if(currentbird == 6 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Go Go Go";
				break;
			case 1 : 
				m_textChant.text = "Soar High!";
				break;
			case 2 : 
				m_textChant.text = "Falcons!";
				break;
			default : 
				m_textChant.text = "Lets go win this fight!";
				break;
			}
		}
		else if(currentbird == 7 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Bitter is Better";
				break;
			case 1 : 
				m_textChant.text = "Dont worry im strong";
				break;
			case 2 : 
				m_textChant.text = "#hugot!";
				break;
			default : 
				m_textChant.text = "Its Complicated";
				break;
			}
		}
		else if(currentbird == 8 ) {
			switch (chatvalue) {
			case 0 : 
				m_textChant.text = "Go Fight";
				break;
			case 1 : 
				m_textChant.text = "Animo";
				break;
			case 2 : 
				m_textChant.text = "Go Fight";
				break;
			default : 
				m_textChant.text = "Lions!";
				break;
			}
		}
	}

	public void Reset()
	{
		m_isAlive = true;
		this.GetComponent<Rigidbody2D> ().gravityScale = 0;

		if (m_objectArrow) {
			//m_objectArrow.transform.parent = GameScene.instance.m_objectRootArrows.transform;
			if( m_objectArrow.GetComponent<Rocket>() )
				m_objectArrow.GetComponent<Rigidbody2D> ().isKinematic = true;
			else
				m_objectArrow.GetComponent<Rigidbody2D> ().isKinematic = false;
			m_objectArrow = null;
		}
		if (m_objectArrow2) {
			//m_objectArrow2.transform.parent = GameScene.instance.m_objectRootArrows.transform;
			if( m_objectArrow2.GetComponent<Rocket>() )
				m_objectArrow2.GetComponent<Rigidbody2D> ().isKinematic = true;
			else
				m_objectArrow2.GetComponent<Rigidbody2D> ().isKinematic = false;
			m_objectArrow2 = null;
		}

		currenthp = hp;


	}
}
