﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum DivisionType
{
	WOOD,
	BRONZE,
	SILVER,
	GOLD,
	PLATINUM,
	MASTER
}

[System.Serializable]
public class DivisionSettings
{
	public string Mode;
	public List<int> ListOfRankIndicator = new List<int> ();
}

public class DivisionManager : ZSingleton<DivisionManager> 
{
	public List<string> m_listOfDivisons 						= new List<string>();
	public List<DivisionSettings> m_listOfDivisionSettings 		= new List<DivisionSettings>();

	public int m_endlessIndex									= 0;
	public int m_levelIndex										= 0;
	public int m_tapIndex										= 0;

	public Image m_imgTrophy	= null;
	public Image m_imgbar										= null;
	public Text m_txtRequirements = null;
	public Text m_txtRank = null;

	void Awake () 
	{
		if (!PlayerPrefs.HasKey ("endless_division")) 
		{
			PlayerPrefs.SetInt ("endless_division", 0);
		} else if (!PlayerPrefs.HasKey ("tap_division")) {
			PlayerPrefs.SetInt ("endless_division", 0);
		} else if (!PlayerPrefs.HasKey ("tap_division")) {
			PlayerPrefs.SetInt ("tap_division", 0);
		}

		LoadData ();
	}

	public void SetDivisionUp(string p_mode)
	{
		switch (p_mode) 
		{
		case "ENDLESS":
			{
				m_endlessIndex++;
				PlayerPrefs.SetInt ("endless_division", m_endlessIndex);
			}
			break;

		case "LEVEL":
			{
				m_levelIndex++;
				PlayerPrefs.SetInt ("endless_division", m_levelIndex);
			}
			break;

		case "TAP":
			{
				m_tapIndex++;
				PlayerPrefs.SetInt ("endless_division", m_tapIndex);
			}
			break;
		}
	}

	public void Refresh(string p_mode, int m_currentRankValue)
	{
		switch (p_mode) 
		{
		case "ENDLESS":
			{
				int currentRank = m_listOfDivisionSettings [0].ListOfRankIndicator [m_endlessIndex];
				m_txtRank.text = "" + m_listOfDivisons[m_endlessIndex];
				m_txtRequirements.text = "" + m_currentRankValue + "/" + currentRank;
				m_imgbar.fillAmount = remapValue (m_currentRankValue, 0, currentRank, 0, 1);
			}
			break;

		case "LEVEL":
			{
				int currentRank = m_listOfDivisionSettings [1].ListOfRankIndicator [m_levelIndex];					
				m_txtRank.text = "" + m_listOfDivisons[m_levelIndex];
				m_txtRequirements.text = "" + (m_currentRankValue - 1) + "/" + currentRank;
				float remappedValue =  remapValue ((m_currentRankValue - 1), 0, currentRank, 0, 1);
				m_imgbar.fillAmount = remappedValue;
				Debug.Log ("Mapped: " + remappedValue);
			}
			break;

		case "TAP":
			{
				int currentRank = m_listOfDivisionSettings [2].ListOfRankIndicator [m_tapIndex];
				m_txtRank.text = "" + m_listOfDivisons[m_tapIndex];
				m_txtRequirements.text = "" + m_currentRankValue + "/" + currentRank;
				m_imgbar.fillAmount = remapValue (m_currentRankValue, 0, currentRank, 0, 1);
			}
			break;
		}
	}

	public int GetCurrentDivision(string p_mode)
	{
		switch (p_mode) 
		{
		case "ENDLESS":
			{
				return m_endlessIndex;
			}
			break;

		case "LEVEL":
			{
				return m_levelIndex;
			}
			break;

		case "TAP":
			{
				return m_tapIndex;
			}
			break;


		default:
			break;
		}
		return 0;
	}

	public int GetCurrentDivisionTarget(string p_mode)
	{
		switch (p_mode) 
		{
		case "ENDLESS":
			{
				return m_listOfDivisionSettings [0].ListOfRankIndicator [m_endlessIndex];
			}
			break;

		case "LEVEL":
			{
				return m_listOfDivisionSettings [1].ListOfRankIndicator [m_levelIndex];
			}
			break;

		case "TAP":
			{
				return m_listOfDivisionSettings [2].ListOfRankIndicator [m_tapIndex];
			}
			break;
		}
		return 0;
	}

	private void LoadData()
	{
		m_endlessIndex = PlayerPrefs.GetInt ("endless_division");
		m_levelIndex = PlayerPrefs.GetInt ("level_division");
		m_tapIndex = PlayerPrefs.GetInt ("tap_division");
	}

	public float remapValue(float p_value, float p_from1, float p_to1, float p_from2, float p_to2)
	{
		return (p_value - p_from1) / (p_to1 - p_from1) * (p_to2 - p_from2) + p_from2;
	}
}
