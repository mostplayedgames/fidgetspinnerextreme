﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AwardScene : MonoBehaviour {

	public static AwardScene Instance;

	public Image m_imgTrophy;
	public Image m_imgProgressBar;
	public Text m_textRank;
	public Text m_textNumberOfGamesPlayed;
	public Text m_textHighScore;

	public int m_iterator;

	public AudioClip m_audioButton;


	public void Awake()
	{
		Instance = this;
	}

	public void Back()
	{
		this.gameObject.SetActive (false);		
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void NextMode()
	{

	}

	public void PrevMode()
	{

	}

	public void Initialize()
	{
		string curMode = GameScene.instance.m_curGameMode;

		switch(curMode)
		{
			case "ENDLESS":
			case "TAP":
			{
				float highscore = GameScene.instance.m_bestScore;
				m_textHighScore.text = "" + highscore;
			}
			break;

			case "LEVEL":
			{
				float curLevel = GameScene.instance.m_curLevel;
				m_textHighScore.text = "" + curLevel;
			}
			break;
		}
	}


}
