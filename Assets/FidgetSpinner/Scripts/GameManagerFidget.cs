﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public enum CONTROL_TYPE
{
	TOUCH,
	SWIPE
}

public enum GAME_STATE_FIDGET
{
	START,
	GAME,
	END
}

public class GameManagerFidget : MonoBehaviour 
{
	public static GameManagerFidget Instance;

	// Use this for initialization

	public Text m_textScore;
	public Text m_textDescription;
	public Text m_textButton;


	public float m_upwardForce;
	public float m_sidewardForce;

	public int m_score;

	private Ray ray;
	private RaycastHit hit;

	private Vector2 m_lateInitialTouch;
	private Vector2 m_initialTouch;
	private Vector2 m_touchReleased;

	public GameObject m_fidgetSpinner;

	public bool m_isSwipe;
	private Vector3 m_mouseDelta;

	public GAME_STATE_FIDGET m_curGameState;

	void Awake()
	{
		Instance = this;

		ResetGame ();
		m_curGameState = GAME_STATE_FIDGET.START;
		//ChangeControl ();
	}

	void Update()
	{
		switch(m_curGameState)
		{
		case GAME_STATE_FIDGET.START:
			{
				StartControls ();
			}
			break;

		case GAME_STATE_FIDGET.GAME:
			{
				OnMouseButtonDown ();
				OnMouseButton ();
				OnMouseButtonUp ();
			}
			break;

		case GAME_STATE_FIDGET.END:
			{
				ResetGame ();
			}
			break;
		}
	}

	public void SetState(GAME_STATE_FIDGET p_newState)
	{
		m_curGameState = p_newState;
	}

	private void StartControls()
	{
		OnMouseButtonDown ();
		if (m_fidgetSpinner != null) 
		{
			m_fidgetSpinner.GetComponent<FidgetSpinner> ().SetSpinValue ();
			SetState (GAME_STATE_FIDGET.GAME);
		}
	}

	private void OnMouseButtonDown()
	{
		ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray, out hit, 10000.0f)) 
		{
			if (Input.GetMouseButtonDown (0)) 
			{
				m_fidgetSpinner = hit.collider.gameObject;
				m_fidgetSpinner.GetComponent<Rigidbody> ().useGravity = false;
				m_initialTouch = hit.point;
				m_mouseDelta = Vector3.zero;
			}
		}
	}

	private void OnMouseButton()
	{
		if (Input.GetMouseButton (0)) 
		{
			if (m_fidgetSpinner == null) {
				return;
			}
			m_touchReleased = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			Vector3 mouseDelta = m_touchReleased - m_initialTouch;
			Debug.Log ("DELTA: " + mouseDelta + " i " + m_touchReleased + " m " + m_initialTouch);

			if (mouseDelta.magnitude > 0.05f) {
				Debug.Log ("HighMagnitude");
				m_isSwipe = true;
				m_mouseDelta = mouseDelta;
			} else {
				m_isSwipe = false;
				m_lateInitialTouch = m_touchReleased;
			}

			m_fidgetSpinner.transform.position = m_touchReleased;
			m_initialTouch = m_touchReleased;

		}
	}

	private void OnMouseButtonUp()
	{
		if (Input.GetMouseButtonUp (0)) 
		{
			if (m_fidgetSpinner == null) 
			{
				return;
			}

			m_fidgetSpinner.GetComponent<Rigidbody> ().useGravity = true;
			m_fidgetSpinner.GetComponent<Rigidbody> ().velocity = Vector3.zero;

			float swipeForce = m_mouseDelta.magnitude;

			if (swipeForce > 1.0f) {
				swipeForce = 1.0f;
			}

			if (m_isSwipe) {
				float idx = m_mouseDelta.sqrMagnitude;
				float testIdx = m_mouseDelta.magnitude;

				if (m_touchReleased.x > m_lateInitialTouch.x + 0.15f) {
					Debug.Log ("Left Side Pressed");
					m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (Vector3.up * m_upwardForce * swipeForce);
					m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (Vector3.right * m_sidewardForce);
				} else if (m_touchReleased.x < m_lateInitialTouch.x - 0.15) {
					Debug.Log ("Right Side Pressed");
					m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (Vector3.up * m_upwardForce * swipeForce);
					m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (-Vector3.right * m_sidewardForce);
				} else {
					Debug.Log ("Center Button is Pressed");
					m_fidgetSpinner.GetComponent<Rigidbody> ().AddForce (Vector3.up * m_upwardForce * swipeForce);
				}

				UpdateScore ();
				m_isSwipe = false;
				m_lateInitialTouch = Vector3.zero;
			}
			m_fidgetSpinner = null;
		}
	}

	private void UpdateScore()
	{
		m_score += 1;
		m_textScore.text = "" + m_score;
	}

	private void ResetGame()
	{
		m_textScore.text = "0";
		m_score = 0;
	}
}
