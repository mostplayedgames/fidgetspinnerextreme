﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Advertisements;

public class FidgetSpinner : MonoBehaviour 
{
	private readonly Vector3 INITAL_POS				= new Vector3(0, 16, 0);
	private readonly float GRAVITY					= 11.8f;
	private float m_gravity 						= -3.0f;
	private readonly float JUMP_HEIGHT				= 10.0f;

	public AudioClip m_audioClip;
	public AudioClip m_audioSpin;
	public AudioClip m_audioDie;
	public AudioClip m_audioCoin;
	public float m_spinValue	= 0.0f;
	public GAME_STATE m_curState = GAME_STATE.START;

	public List<Sprite> m_listOfSpinners = new List<Sprite>();

	private float m_countdownTime = 5.0f;
	private float m_timer;

	public bool m_isHolding;

	public Vector3 m_vecUpdatedPosition;
	public Vector3 m_vecLatePosition;

	// Use this for initialization
	void Awake () 
	{
	}
	
	void Update () 
	{
		//m_vecUpdatedPosition = this.transform.position;
		//Vector3 pos = m_vecUpdatedPosition - m_vecLatePosition;

		if (Input.GetKeyDown (KeyCode.L)) 
		{
			Vector3 tempo = Camera.main.WorldToScreenPoint (this.transform.position);
			Debug.Log("POS: " + tempo + " HI: " + Screen.height);
		}


		MakeSpin ();
		CheckIfDead ();

		IsSpinnerHolding ();
		//m_vecLatePosition = this.transform.position;
	}

	public bool IsTouchable(bool m_isFirstTouch)
	{
		if (!m_isFirstTouch)
			return true;

		return (this.GetComponent<Rigidbody> ().velocity.y < -25.0f) ? true : false;
	}

	public void SetSpinner(int p_curSpinner)
	{
		this.transform.GetChild (0).GetComponent<SpriteRenderer> ().sprite = m_listOfSpinners [p_curSpinner];
	}

	public void SetSpinValue()
	{
		m_spinValue = 1000.0f;
		ZAudioMgr.Instance.PlaySFX (m_audioSpin, true, 0, 1);
	}

	public void SetIsHolding(bool p_isHolding)
	{
		m_isHolding = p_isHolding;

		if (m_isHolding) 
		{
			ZAudioMgr.Instance.StopSFX (m_audioSpin);
			ZAudioMgr.Instance.PlaySFX (m_audioSpin, true, 0, 5.0f);
		} 
		else 
		{
			ZAudioMgr.Instance.StopSFX (m_audioSpin);
			ZAudioMgr.Instance.PlaySFX (m_audioSpin, true, 0, 0.8f);
		}
	}

	private void IsSpinnerHolding()
	{
		if (GameScene.instance.m_eState != GAME_STATE.GAME)
			return;
		
		if (!m_isHolding) 
		{
			m_timer = 0;
			m_spinValue = 1000.0f;
			this.GetComponent<Animator> ().Play ("New State");
		} 
		else 
		{
			m_timer += 1.0f * Time.deltaTime;
			m_spinValue -= 100.0f * Time.deltaTime;

			if (m_timer > m_countdownTime) {
				//RestartLevel ();
				GameScene.instance.m_forcedReleaseTouch = true;
				m_spinValue = 0.0f;
				m_timer = 0;
			} 
			else if (m_timer > m_countdownTime - 2) 
			{
				this.GetComponent<Animator> ().Play ("RedBlink");
			}
		}
	}

	private void MakeSpin()
	{
		Vector3 rotationValue = new Vector3 (0, 0, m_spinValue * Time.deltaTime);
		this.transform.Rotate (rotationValue);
		//this.GetComponent<Rigidbody> ().AddRelativeTorque(rotationValue);
	}

	private void Gravity()
	{
		m_gravity -= GRAVITY * Time.deltaTime;
		this.transform.position += new Vector3 (0,
			m_gravity * Time.deltaTime, 
			0);
	}

	private void CheckIfDead()
	{
		Vector3 screenPointPos = Camera.main.WorldToScreenPoint (this.transform.position);

		if (screenPointPos.y < -400 ||
			screenPointPos.x > Screen.width + 300 ||
			screenPointPos.x < -300) 
		{
			ZCameraMgr.instance.DoShake();
			LeanTween.delayedCall(0.5f, RestartLevel);
			this.transform.position = new Vector3(0, 99,99);
			ZAudioMgr.Instance.PlaySFX (m_audioDie);
			ZAudioMgr.Instance.StopSFX (m_audioSpin);
			GameScene.instance.ShowFailText ();
		}
	}

	public void AnimateFidget()
	{
//		LeanTween.delayedCall (0.2f, GravityOff);
//		LeanTween.moveLocalY (this.gameObject, 50.0f, 1.0f);
		//this.GetComponent<Animator> ().Play ("Success");
	}

//	public void GravityOff()
//	{
//		this.GetComponent<Rigidbody> ().useGravity = false;
//	}

	public void ResetSpinner()
	{
		//GameManagerFidget.Instance.SetState (GAME_STATE_FIDGET.START);
		this.GetComponent<Animator> ().Play ("New State");
		this.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		this.GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
		this.GetComponent<Rigidbody> ().useGravity = false;
		this.transform.eulerAngles = Vector3.zero;
		m_spinValue = 0.0f;
		this.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.white;
		this.gameObject.SetActive (false);
		this.gameObject.SetActive (true);
		m_timer = 0;
		ZAudioMgr.Instance.StopSFX (m_audioSpin);
	}

	public void RestartLevel()
	{
		ResetSpinner ();
		GameScene.instance.SetState (GAME_STATE.END);
		//GameScene.instance.StartGame ();
	}

	public void OnTriggerEnter(Collider p_other)
	{
		if (m_isHolding)
			return;
		
		if (p_other.tag == "Coin") 
		{
			p_other.gameObject.SetActive (false);
			GameScene.instance.UpdateTopBar ();
			ZAudioMgr.Instance.PlaySFX (m_audioCoin);//, false,0,0.3f);
			CoinManager.Instance.AnimatePlusOne ();
		}
		if (p_other.tag == "Base") 
		{
//			this.GetComponent<Rigidbody> ().AddForce (Vector3.up * 9999.0f);
//			this.GetComponent<Rigidbody> ().AddForce (Vector3.right * 999.0f);
		}
	}

}
