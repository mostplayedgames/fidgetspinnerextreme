﻿using UnityEngine;
using System.Collections;

public class ZHeartbeatAnim : MonoBehaviour {

	public float scaleUp = 1.3f;
	public float defaultScale = 1;
	public float timeScale = 0.2f;
	public float timeHeartbeat = 2f;

	// Use this for initialization
	void Start () {
		this.transform.localScale = new Vector3 (defaultScale, defaultScale, defaultScale);
		Heartbeat ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void Heartbeat()
	{
		LeanTween.scale (this.gameObject, new Vector3(scaleUp,scaleUp,scaleUp), timeScale).setOnComplete (HeartbeatDown);
	}

	void HeartbeatDown()
	{
			LeanTween.scale (this.gameObject, new Vector3(defaultScale,defaultScale,defaultScale), timeScale).setOnComplete (Wait);
	}

	void Wait()
	{
		LeanTween.delayedCall (timeHeartbeat, Heartbeat);
	}
}
