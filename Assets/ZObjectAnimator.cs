﻿using UnityEngine;
using System.Collections;

public enum ANIM_TYPE
{
	SHAKE_ONCE,
	LOOP_ALPHA_INOUT,
	NONE
}

public class ZObjectAnimator : MonoBehaviour {

	public ANIM_TYPE animType;

	public float value;
	public float time;
	public LeanTweenType tweenType;

	// Use this for initialization
	void OnEnable () {
		Vector2 currentScale = this.transform.localScale;
		switch(animType)
		{
		case ANIM_TYPE.LOOP_ALPHA_INOUT :
			LeanTween.alpha(this.gameObject, value, time).setEase(tweenType).setLoopPingPong();
			break;
		case ANIM_TYPE.SHAKE_ONCE :
			LeanTween.rotateZ(this.gameObject, value, time).setEase(LeanTweenType.easeInOutElastic).setLoopPingPong().setLoopCount(2);
			break;
		}
	}
}
