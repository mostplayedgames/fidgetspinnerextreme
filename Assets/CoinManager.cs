﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour {

	public static CoinManager Instance;

	public GameObject m_objectCoin;
	public Color m_coinDefaultColor;

	public Vector2 m_minVec = new Vector2(-5, 5);
	public Vector2 m_maxVec = new Vector2(5, 8.5f);

	public float m_countodownTimer;
	public float m_targetTime = 5.0f;

	public GameObject m_objectPlusOne;
	public GameObject m_objectSuccessParticle;

	// Use this for initialization
	void Awake () 
	{
		Instance = this;
	}

	private void RandomSpawn()
	{
		m_objectCoin.gameObject.SetActive (true);
		float randValX = Random.Range (m_minVec.x, m_maxVec.x);
		float randValY = Random.Range (m_minVec.y, m_maxVec.y);

		m_objectSuccessParticle.gameObject.SetActive (false);
		m_objectCoin.transform.position = new Vector3 (randValX, randValY, 0);
		LeanTween.cancel (m_objectCoin.gameObject);
		LeanTween.scale (m_objectCoin.gameObject, m_objectCoin.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.1f).setEase (LeanTweenType.easeInOutCubic).setLoopCount (2).setLoopPingPong ();

		ZAudioMgr.Instance.PlaySFX (GameScene.instance.m_audioButton);
	}

	public void AnimatePlusOne()
	{
		m_objectPlusOne.transform.position = m_objectCoin.transform.position;
		m_objectPlusOne.GetComponent<SpriteRenderer>().color = m_coinDefaultColor;
		LeanTween.alpha (m_objectPlusOne, 0, 0.5f);
		LeanTween.moveLocalY (m_objectPlusOne.gameObject, m_objectPlusOne.transform.localPosition.y + 2.0f, 0.5f);

		m_objectSuccessParticle.gameObject.SetActive (true);
		m_objectSuccessParticle.transform.position = m_objectCoin.transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (GameScene.instance.m_eState != GAME_STATE.GAME) 
		{
			m_objectCoin.gameObject.SetActive (false);
			m_countodownTimer = 0;
			return;
		}

		if (m_objectCoin.activeSelf) 
		{
			return;
		}

		
		if (m_targetTime <= m_countodownTimer) 
		{
			if (Input.GetMouseButton (0)) {
				RandomSpawn ();
				m_countodownTimer = 0;
			}
		} 
		else 
		{
			m_countodownTimer += 1.0f * Time.deltaTime;

		}
	}
}
