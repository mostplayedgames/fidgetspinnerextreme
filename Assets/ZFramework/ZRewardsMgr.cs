﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.Advertisements;

public enum REWARD_TYPES
{
	SHOP,
	SHARE,
	RATEUS,
	MOREGAMES,
	WATCHVIDEO,
	LIKEUS,
	FOLLOWTWITCH,
	SUBSCRIBE,
	FOLLOWUS,
	LEADERBOARD,
	CHANGEMODE,
}

[System.Serializable]
public class ZRewardData{
	public REWARD_TYPES types;
	public string displayName;
	public Sprite icon;
	public Color baseColor;
	public Color highlightColor;
	public int rewardValue;
	public int timeInitial;
	public int timeReccuring;
}

public class ZRewardsMgr : MonoBehaviour {

	public static ZRewardsMgr instance;

	public ZRewardData m_rewardNewChar;
	public ZRewardData m_rewardWatchVideo;

	public ZRewardData m_rewardShare;
	public ZRewardData m_rewardLikeUs;
	public ZRewardData m_rewardFollowUs;
	public ZRewardData m_rewardFollowTwitch;
	public ZRewardData m_rewardSubscribe;
	public ZRewardData m_rewardMoreGames;
	public ZRewardData m_rewardRateUs;
	public ZRewardData m_rewardLeaderboard;
	public ZRewardData m_rewardShop;
	public ZRewardData m_rewardChangeMode;

	public GameObject m_objectGetCash;
	public GameObject m_objectShop;
	public GameObject m_objectCharAvail;

	public GameObject m_objectButtons;
	public Image m_imageButton;
	public Image m_imageIcon;
	public Text m_text;
	public Text m_textEarned;

	public AudioClip m_audioButton;

	REWARD_TYPES m_currentRewardType;
	ZRewardData m_currentRewardData;

	float m_currentIncentifiedAdsTime = 30;
	float m_currentRateUsTime = 30;
	float m_currentLikeUsTime = 30;
	float m_currentMoreGamesTime = 30;
	float m_currentFollowUsTime = 30;
	float m_currentFollowTwitchTime = 30;
	float m_currentSubscribeTime =30;
	float m_currentShareTime = 30;
	float m_currentLeaderboardTime = 30;
	float m_currentShopTime = 30;
	float m_currentChangeModeTime = 30;

	Color m_colorBase;
	Color m_colorHighlight;
	bool m_startInterrupt;
	int m_currentRewardValue;

	// Use this for initialization
	void Start () {

		instance = this;

		if (!PlayerPrefs.HasKey ("rewardsShare")) {
			PlayerPrefs.SetInt ("rewardsShare", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardLikeUs")) {
			PlayerPrefs.SetInt ("rewardLikeUs", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardFollowUs")) {
			PlayerPrefs.SetInt ("rewardFollowUs", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardFollowTwitch")) {
			PlayerPrefs.SetInt ("rewardFollowTwitch", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardSubscribe")) {
			PlayerPrefs.SetInt ("rewardSubscribe", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardMoreGames")) {
			PlayerPrefs.SetInt ("rewardMoreGames", 0);
		}

		if (!PlayerPrefs.HasKey ("rewardRateUs")) {
			PlayerPrefs.SetInt ("rewardRateUs", 0);
		}

		m_currentIncentifiedAdsTime = m_rewardWatchVideo.timeInitial;//30;//180;
		m_currentRateUsTime = m_rewardRateUs.timeInitial;
		m_currentLikeUsTime = m_rewardLikeUs.timeInitial;
		m_currentMoreGamesTime = m_rewardMoreGames.timeInitial;
		m_currentFollowUsTime = m_rewardFollowUs.timeInitial;
		m_currentFollowTwitchTime = m_rewardFollowTwitch.timeInitial;
		m_currentSubscribeTime = m_rewardSubscribe.timeInitial;
		m_currentShareTime = m_rewardShare.timeInitial;
		m_currentLeaderboardTime = m_rewardLeaderboard.timeInitial;
		m_currentShopTime = m_rewardShop.timeInitial;
		m_currentChangeModeTime = m_rewardChangeMode.timeInitial;

		m_startInterrupt = false;
	}
	
	// Update is called once per frame
	void Update () {
		m_currentIncentifiedAdsTime -= Time.deltaTime;
		m_currentRateUsTime -= Time.deltaTime;
		m_currentLikeUsTime -= Time.deltaTime;
		m_currentMoreGamesTime -= Time.deltaTime;
		m_currentFollowUsTime -= Time.deltaTime;
		m_currentFollowTwitchTime -= Time.deltaTime;
		m_currentSubscribeTime -= Time.deltaTime;
		m_currentShareTime -= Time.deltaTime;
		m_currentLeaderboardTime -= Time.deltaTime;
		m_currentShopTime -= Time.deltaTime;
		m_currentChangeModeTime -= Time.deltaTime;
	}

	public bool WillShowShopButton()
	{
		if (m_currentShopTime <= 0)
			return true;
		return false;
	}

	public bool WillShowReward()
	{
		if (m_currentIncentifiedAdsTime <= 0)
			return true;
		else if (m_currentLeaderboardTime <= 0)
			return true;
		else if (m_currentChangeModeTime <= 0)
			return true;
		return false;
	}

	public bool WillShowRewardSuccess()
	{
		if (m_currentRateUsTime <= 0 && PlayerPrefs.GetInt ("rewardRateUs") < ZGameMgr.instance.UPDATE_NUMBER)
			return true;
		else if (m_currentLikeUsTime <= 0 && PlayerPrefs.GetInt ("rewardLikeUs") <= 0)
			return true;
		else if (m_currentMoreGamesTime <= 0 && PlayerPrefs.GetInt ("rewardMoreGames") <= 0)
			return true;
		else if (m_currentFollowUsTime <= 0 && PlayerPrefs.GetInt ("rewardFollowUs") <= 0)
			return true;
		else if (m_currentFollowTwitchTime <= 0 && PlayerPrefs.GetInt ("rewardFollowTwitch") <= 0)
			return true;
		else if (m_currentSubscribeTime <= 0 && PlayerPrefs.GetInt ("rewardSubscribe") <= 0)
			return true;
		else if (m_currentShareTime <= 0)
			return true;
		else if (m_currentLeaderboardTime <= 0)
			return true;
		return false;
	}

	public void ShowShopButton(bool force = false){
		if (m_currentShopTime <= 0 || force) {
			m_currentShopTime = m_rewardShop.timeReccuring;//30;//180;
			//ShowButton (m_rewardShop);

			m_objectCharAvail.SetActive (true);
			//LeanTween.cancel (m_objectCharAvail);
			//LeanTween.scale (m_objectCharAvail, m_objectCharAvail.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
			//AnimateButton ();

			LeanTween.delayedCall (2f, ShowShopButtonRoutine);

			m_currentRewardType = REWARD_TYPES.SHOP;
		}
	}

	void ShowShopButtonRoutine()
	{
		//m_objectShop.SetActive (true);
		//m_objectShop.GetComponent<ShopScene> ().SetupScene ();
		m_objectCharAvail.SetActive (false);
	}

	public void ShowReward(){

		// SHOW REWARDED AD
		if (m_currentIncentifiedAdsTime <= 0) {
			m_currentIncentifiedAdsTime = m_rewardWatchVideo.timeReccuring;//30;//180;
			ShowButton (m_rewardWatchVideo);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.WATCHVIDEO;
		} 
		// LEADERBOARD
		else if (m_currentLeaderboardTime <= 0 ) {
			m_currentLeaderboardTime = m_rewardLeaderboard.timeReccuring;

			ShowButton (m_rewardLeaderboard);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.LEADERBOARD;
		} 
		// CHANGEMODE
		else if (m_currentChangeModeTime <= 0 ) {
			m_currentChangeModeTime = m_rewardChangeMode.timeReccuring;

			ShowButton (m_rewardChangeMode);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.CHANGEMODE;
		} 

	}

	public void ShowRewardSuccess(){

		if (m_currentRateUsTime <= 0 && PlayerPrefs.GetInt ("rewardRateUs") < ZGameMgr.instance.UPDATE_NUMBER) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring + 120;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			ShowButton (m_rewardRateUs);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.RATEUS;
		}

		// LIKE US
		else if (m_currentLikeUsTime <= 0 && PlayerPrefs.GetInt ("rewardLikeUs") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring + 120;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			ShowButton (m_rewardLikeUs);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.LIKEUS;
		}	

		// MORE GAMES
		else if (m_currentMoreGamesTime <= 0 && PlayerPrefs.GetInt ("rewardMoreGames") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring + 120;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			ShowButton (m_rewardMoreGames);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.MOREGAMES;
		}

		// FOLLOW US
		else if (m_currentFollowUsTime <= 0 && PlayerPrefs.GetInt ("rewardFollowUs") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring + 120;
			ShowButton (m_rewardFollowUs);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.FOLLOWUS;
		}

		// FOLLOW TWITCH
		else if (m_currentFollowTwitchTime <= 0 && PlayerPrefs.GetInt ("rewardFollowTwitch") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			m_currentFollowTwitchTime = m_rewardFollowTwitch.timeReccuring + 120;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring;
			ShowButton (m_rewardFollowUs);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.FOLLOWUS;
		}


		// SUBSCRIBE
		else if (m_currentFollowUsTime <= 0 && PlayerPrefs.GetInt ("rewardSubscribe") <= 0) {
			m_currentRateUsTime = m_rewardRateUs.timeReccuring;
			m_currentLikeUsTime = m_rewardLikeUs.timeReccuring;
			m_currentMoreGamesTime = m_rewardMoreGames.timeReccuring;
			m_currentFollowUsTime = m_rewardFollowUs.timeReccuring;
			m_currentFollowTwitchTime = m_rewardFollowTwitch.timeReccuring;
			m_currentSubscribeTime = m_rewardSubscribe.timeReccuring + 120;
			ShowButton (m_rewardFollowUs);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.FOLLOWUS;
		}


		// SHARE
		else if (m_currentShareTime <= 0 ) {
			m_currentShareTime = m_rewardShare.timeReccuring;

			ShowButton (m_rewardShare);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.SHARE;
		} 

		// LEADERBOARD
		else if (m_currentLeaderboardTime <= 0 ) {
			m_currentLeaderboardTime = m_rewardLeaderboard.timeReccuring;

			ShowButton (m_rewardLeaderboard);

			AnimateButton ();

			m_currentRewardType = REWARD_TYPES.LEADERBOARD;
		} 
	}

	void AnimateButton()
	{
		LeanTween.cancel (m_objectButtons);
		LeanTween.scale (m_objectButtons, m_objectButtons.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
	}

	void ShowButton(ZRewardData data)
	{
		m_objectButtons.SetActive (true);

		m_imageIcon.overrideSprite = data.icon;
		m_imageButton.color = data.baseColor;
		if (data.rewardValue > 0) {
			m_textEarned.text = "EARN +" + data.rewardValue;
			m_text.text = data.displayName;
		} else {
			m_textEarned.text = data.displayName;
			m_text.text = "CHECK NOW";
		}


		
		
		m_colorBase = data.baseColor;
		m_colorHighlight = data.highlightColor;

		m_currentRewardData = data;
		m_currentRewardValue = data.rewardValue;

		ShowButtonAnimate ();
	}

	void ShowButtonAnimate()
	{
		if (!m_objectButtons.activeSelf)
			return;

		m_imageButton.color = m_colorBase;
		LeanTween.delayedCall (0.5f, ShowButtonAnimateDown);
	}

	void ShowButtonAnimateDown()
	{
		if (!m_objectButtons.activeSelf)
			return;

		m_imageButton.color = m_colorHighlight;
		LeanTween.delayedCall (0.5f, ShowButtonAnimate);
	}

	public void ActionButton(){
		m_objectButtons.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		switch (m_currentRewardType) 
		{
		case REWARD_TYPES.FOLLOWUS:
			ZFollowUsMgr.Instance.FollowUs ();
			//PlayerPrefs.SetInt ("rewardFollowUs", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.LIKEUS:
			ZFollowUsMgr.Instance.LikeUs();
			//PlayerPrefs.SetInt ("rewardLikeUs", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.FOLLOWTWITCH:
			ZFollowUsMgr.Instance.FollowTwitch();
			//PlayerPrefs.SetInt ("rewardLikeUs", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.SUBSCRIBE:
			ZFollowUsMgr.Instance.Subscribe();
			//PlayerPrefs.SetInt ("rewardLikeUs", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.MOREGAMES:
			ZFollowUsMgr.Instance.MoreGames();
			//PlayerPrefs.SetInt ("rewardMoreGames", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.RATEUS:
			ZRateUsMgr.Instance.RateUs();
			//PlayerPrefs.SetInt ("rewardRateUs", ZGameMgr.instance.UPDATE_NUMBER);
			m_startInterrupt = true;
			ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.SHARE:
			ZFollowUsMgr.Instance.Share();
			ZAnalytics.Instance.SendShareComplete ();
			//m_startInterrupt = true;
			//ZGameMgr.instance.m_startInterrupt = true;
			break;
		case REWARD_TYPES.SHOP:
			//m_objectShop.SetActive (true);
			//ShopScene.instance.SetupScene();
			//ZAnalytics.Instance.SendShopComplete ();
			break;
		case REWARD_TYPES.WATCHVIDEO:
			ShowRewardedVideo ();
			break;
		case REWARD_TYPES.CHANGEMODE:
			GameScene.instance.ChangeMode ();
			break;
		case REWARD_TYPES.LEADERBOARD:
			if (ZGameMgr.instance.isOnline) {
				GameScene.instance.OpenLeaderboard ();
			} else {
				// No Connection
				GameScene.instance.m_objectNoInternet.SetActive(true);
			}
			break;
		}

	}

	void OnApplicationFocus( bool focusStatus )
	{
		if( !m_startInterrupt )
			return;
			
		RewardPlayer ();
		m_startInterrupt = false;

		switch (m_currentRewardType) {
		case REWARD_TYPES.FOLLOWUS:
			PlayerPrefs.SetInt ("rewardFollowUs", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendFollowUsComplete ();
			break;
		case REWARD_TYPES.LIKEUS:
			PlayerPrefs.SetInt ("rewardLikeUs", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendLikeUsComplete ();
			break;
		case REWARD_TYPES.FOLLOWTWITCH:
			PlayerPrefs.SetInt ("rewardFollowTwitch", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendFollowTwitchComplete ();
			break;
		case REWARD_TYPES.SUBSCRIBE:
			PlayerPrefs.SetInt ("rewardSubscribe", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendSubscribeComplete ();
			break;
		case REWARD_TYPES.MOREGAMES:
			PlayerPrefs.SetInt ("rewardMoreGames", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendMoreGamesComplete ();
			break;
		case REWARD_TYPES.RATEUS:
			PlayerPrefs.SetInt ("rewardRateUs", ZGameMgr.instance.UPDATE_NUMBER);
			ZAnalytics.Instance.SendRateUsComplete ();
			break;
		}
	}
		
	void OpenLeaderboard()
	{
		#if UNITY_ANDROID
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[PlayerPrefs.GetInt ("Mode")].leaderboardAndroid);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	}
		
	void RewardPlayer()
	{
		if (ZGameMgr.instance.isOnline) {
			LeanTween.delayedCall (0.5f, RewardPlayerRoutine);
		} else {
			// No Connection
			GameScene.instance.m_objectNoInternet.SetActive(true);
		}
	}

	void RewardPlayerRoutine()
	{
		ZAnalytics.Instance.SendEarnCoinsEvent ("reward" + m_currentRewardType, m_currentRewardValue);
		GameScene.instance.AddCoins (m_currentRewardValue);
		//m_objectGetCash.SetActive (true);
		//m_textEarned.text = "+" + m_currentRewardValue;
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		//LeanTween.scale (m_objectGetCash, m_objectGetCash.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
	}

	void ShowShop()
	{
		m_objectShop.SetActive (true);
	}


	public void ShowRewardedVideo()
	{
		if (Advertisement.IsReady ("rewardedVideo")) {
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		}
		else {
			// No Connection
			GameScene.instance.m_objectNoInternet.SetActive(true);
		}
		//m_objectWatchVideo.SetActive (false);

		//ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		//if (ZAdsMgr.Instance.removeAds > 0)
		//	return;
		//if( isRateUs > 0 )
		//	return;
		//if (!Advertisement.IsReady ("rewardedVideo"))
		//	return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
//			ShowOptions options = new ShowOptions();
//			options.resultCallback = AdCallbackhanler;
//			Advertisement.Show ("rewardedVideo", options);
		} else 
		{
			//LeanTween.delayedCall (1f, SetupLevel);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			RewardPlayer ();
			ZAnalytics.Instance.SendEarnComplete ();
			//if (rewardedSource == 0)
			//	ContinueLevel ();
			//if (rewardedSource == 1) {
				//AddCoins (100);
				//m_objectEarned.SetActive (true);
				//new MobileNativeMessage("You get 100 Eagles", "Thank you for your Support!");
				//m_objectTulong.SetActive (true);
			//}
			//m_currentAds = 0;
			break;
		case ShowResult.Skipped:
			//LeanTween.delayedCall (1f, SetupLevel);
			break;
		case ShowResult.Failed:
			//LeanTween.delayedCall (1f, SetupLevel);
			break;
		}
	}
}
