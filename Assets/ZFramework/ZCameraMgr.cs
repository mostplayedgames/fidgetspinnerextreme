﻿using UnityEngine;
using System.Collections;

public class ZCameraMgr : MonoBehaviour {

	public static ZCameraMgr instance;

	public bool Shaking; 
	private float ShakeDecay;
	private float ShakeIntensity;    
	private Vector3 OriginalPos;
	private Quaternion OriginalRot;

	private bool m_isTweening;
	private float m_targetVal;
	private float m_timer;
	private float m_start;

	void Start()
	{
		instance = this;
		Shaking = false;   
	}

	void Update () 
	{
		if (m_isTweening) 
		{
			m_timer += 2.0f * Time.deltaTime;

			float tim =  Mathf.Lerp (m_start, m_targetVal, m_timer);

			this.GetComponent<Camera> ().orthographicSize = tim;
		
			if (m_timer >= m_targetVal) 
			{
				Debug.Log ("SECRET");
				m_isTweening = false;
				m_timer = 0;
			}
		}

		if(ShakeIntensity > 0)
		{
			transform.position = OriginalPos + Random.insideUnitSphere * ShakeIntensity;
			transform.rotation = new Quaternion(OriginalRot.x + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
				OriginalRot.y + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
				OriginalRot.z + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
				OriginalRot.w + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f);

			ShakeIntensity -= ShakeDecay;
		}
		else if (Shaking)
		{
			Shaking = false;  
			transform.position = OriginalPos;
			transform.rotation = OriginalRot;
		}
	}

	public void SetOrtographicSize(float p_value)
	{
		//this.GetComponent<Camera> ().orthographicSize = p_value;
		m_isTweening = true;
		m_targetVal = p_value;
		m_start = this.GetComponent<Camera> ().orthographicSize;
		m_timer = 0;
	}

	public void DoShake()
	{
		OriginalPos = transform.position;
		OriginalRot = transform.rotation;

		ShakeIntensity = 0.1f;
		ShakeDecay = 0.01f;
		Shaking = true;
	}    
}
