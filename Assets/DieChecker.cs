﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class DieChecker : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D coll) {
		GameScene.instance.Die ();
		coll.gameObject.SetActive(false);
	}
}
